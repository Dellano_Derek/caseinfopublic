﻿using CaseInfoPublic.Models;
using CaseInfoPublic.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace CaseInfoPublic.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICaseInfoPublicRepository _caseInfoPublicRepository;

        public HomeController(ILogger<HomeController> logger, ICaseInfoPublicRepository infoPublicRepository)
        {
            _logger = logger;
            _caseInfoPublicRepository = infoPublicRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Programa()
        {
            var programas = _caseInfoPublicRepository.GetAllProgramas();

            return View("Programa", programas);
        }

        [HttpGet]
        public IActionResult EditarPrograma(ProgramaModel programa)
        {
            return View("EditarPrograma", programa);
        }

        [HttpPost]
        public IActionResult EditarProgramaPut(ProgramaModel programa)
        {
            _caseInfoPublicRepository.UpdatePrograma(programa.Codigo, programa);

            return Redirect("Programa");
        }

        [HttpGet]
        public IActionResult AdicionarPrograma(bool retomar = false) 
        {
            ViewBag.Retomar = retomar;

            return View("AdicionarPrograma", new ProgramaModel());
        }


        [HttpPost]
        public IActionResult AdicionarProgramaPost(ProgramaModel programa, bool retomar)
        {
            _caseInfoPublicRepository.CreatePrograma(programa);

            if (retomar)
                return Redirect("AdicionarIndicadorPrograma");

            return Redirect("Programa");
        }

        [HttpPost]
        public IActionResult DeletarPrograma(int codigo)
        {
            _caseInfoPublicRepository.DeletePrograma(codigo);

            return Ok();
        }

        [HttpGet]
        public IActionResult IndicadorPrograma()
        {
            var indicadoresPrograma = _caseInfoPublicRepository.GetAllIndicadoresPrograma();

            return View("IndicadorPrograma", indicadoresPrograma);
        }

        [HttpGet]
        public IActionResult EditarIndicadorPrograma(IndicadorProgramaModel indicadorPrograma)
        {
            return View("EditarIndicadorPrograma", indicadorPrograma);
        }

        [HttpPost]
        public IActionResult EditarIndicadorProgramaPut(IndicadorProgramaModel indicadorPrograma)
        {
            _caseInfoPublicRepository.UpdateIndicadorPrograma(indicadorPrograma.IdIndicador, indicadorPrograma);

            return Redirect("IndicadorPrograma");
        }

        [HttpGet]
        public IActionResult AdicionarIndicadorPrograma()
        {
            var programasSemIndicadores = GetProgramasSemIndicadores();

            if (!programasSemIndicadores.Any())
            {
                TempData["Alerta"] = "Não há programas disponíveis sem indicadores associados. Por favor, adicione um programa sem indicadores primeiro.";

                return RedirectToAction("AdicionarPrograma", new { retomar = true });
            }

            return View("AdicionarIndicadorPrograma", programasSemIndicadores);
        }

        [HttpPost]
        public IActionResult AdicionarIndicadorProgramaPost(IndicadorProgramaModel indicadorPrograma)
        {
            _caseInfoPublicRepository.CreateIndicadorPrograma(indicadorPrograma);

            return Redirect("IndicadorPrograma");
        }

        [HttpPost]
        public IActionResult DeletarIndicadorPrograma(int idIndicador)
        {
            _caseInfoPublicRepository.DeleteIndicadorPrograma(idIndicador);

            return Ok();
        }

        [HttpGet]
        public IActionResult ObjetivoPrograma()
        {
            var objetivosPrograma = _caseInfoPublicRepository.GetAllObjetivosPrograma();

            return View("ObjetivoPrograma", objetivosPrograma);
        }
        [HttpGet]
        public IActionResult EditarObjetivoPrograma(ObjetivoProgramaModel ObjetivoPrograma)
        {
            return View("EditarObjetivoPrograma", ObjetivoPrograma);
        }

        [HttpPost]
        public IActionResult EditarObjetivoProgramaPut(ObjetivoProgramaModel objetivoPrograma)
        {
            _caseInfoPublicRepository.UpdateObjetivoPrograma(objetivoPrograma.IdObjetivo, objetivoPrograma);

            return Redirect("ObjetivoPrograma");
        }

        [HttpGet]
        public IActionResult AdicionarObjetivoPrograma()
        {
            var programas = _caseInfoPublicRepository.GetAllProgramas();

            if (!programas.Any())
            {
                TempData["Alerta"] = "Não há programas cadastrados. Por favor, adicione um programa para poder prosseguir.";

                return Redirect("AdicionarPrograma");
            }

            return View("AdicionarObjetivoPrograma", programas);
        }

        [HttpPost]
        public IActionResult AdicionarObjetivoProgramaPost(ObjetivoProgramaModel objetivoPrograma)
        {
            _caseInfoPublicRepository.CreateObjetivoPrograma(objetivoPrograma.IdObjetivo, objetivoPrograma);

            return Redirect("ObjetivoPrograma");
        }

        [HttpPost]
        public IActionResult DeletarObjetivoPrograma(int idObjetivo)
        {
            _caseInfoPublicRepository.DeleteObjetivoPrograma(idObjetivo);

            return Ok();
        }

        // Método para obter programas sem indicadores associados
        private List<ProgramaModel> GetProgramasSemIndicadores()
        {
            var programas = _caseInfoPublicRepository.GetAllProgramas();
            var indicadoresProgramas = _caseInfoPublicRepository.GetAllIndicadoresPrograma();

            // Obtém uma lista de IDs de programas que estão sendo referenciados na tabela de indicadores de programa
            var programasComIndicadores = indicadoresProgramas.Select(ip => ip.IdPrograma).Distinct();

            // Obtém uma lista de programas que não estão sendo referenciados na tabela de indicadores de programa
            var programasSemIndicadores = programas.Where(p => !programasComIndicadores.Contains(p.Codigo)).ToList();

            return programasSemIndicadores;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}