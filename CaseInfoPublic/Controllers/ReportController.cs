﻿using AspNetCore.Reporting;
using CaseInfoPublic.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;

namespace CaseInfoPublic.Controllers
{
    public class ReportController : Controller
    {
        private readonly IWebHostEnvironment _iwebhostenvironment;
        private readonly ICaseInfoPublicRepository _caseInfoPublicRepository;

        public ReportController(IWebHostEnvironment iwebhostenvironment, ICaseInfoPublicRepository caseInfoPublicRepository)
        {
            _iwebhostenvironment = iwebhostenvironment;
            _caseInfoPublicRepository = caseInfoPublicRepository;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GerarRelatorioProgramas() 
        {
            var programas = _caseInfoPublicRepository.GetAllProgramas();

            // Mapear os dados para o formato necessário para o relatório
            var relatorioProgramas = programas.Select(p => new
            {
                p.Codigo,
                p.Descricao,
                p.PublicoAlvo,
                p.Tipo,
                p.ObjMilenio
            }).ToList();
            var path = $"{_iwebhostenvironment.WebRootPath}\\Reports\\ProgramasReport.rdlc";
            LocalReport localReport = new(path);
            localReport.AddDataSource("DataSetProgramas", relatorioProgramas);

            // Parâmetros opcionais para o relatório (se necessário)
                // Dictionary<string, string> parameters = new();

            var result = localReport.Execute(RenderType.Pdf, 1, null, "");

            return File(result.MainStream, "application/pdf", "RelatorioProgramas.pdf");
        }

        public IActionResult GerarRelatorioIndicadoresPrograma()
        {
            var indicadoresPrograma = _caseInfoPublicRepository.GetAllIndicadoresPrograma();

            // Mapear os dados para o formato necessário para o relatório
            var relatorioIndicadoresPrograma = indicadoresPrograma.Select(p => new
            {
                p.IdIndicador,
                p.IdPrograma,
                p.Descricao,
                p.UndMedida,
                p.IndiceApuracao,
                p.IndiceDesejado,
                p.DataApuracao
            }).ToList();

            var path = $"{_iwebhostenvironment.WebRootPath}\\Reports\\IndicadoresProgramaReport.rdlc";

            LocalReport localReport = new(path);

            // Adicionar o ReportDataSource ao LocalReport
            localReport.AddDataSource("DataSetIndicadoresPrograma", relatorioIndicadoresPrograma);

            var result = localReport.Execute(RenderType.Pdf, 1, null, "");

            return File(result.MainStream, "application/pdf", "RelatorioProgramas.pdf");
        }

        public IActionResult GerarRelatorioObjetivosPrograma()
        {
            var objetivos = _caseInfoPublicRepository.GetAllObjetivosPrograma();

            // Mapear os dados para o formato necessário para o relatório
            var relatorioObjetivosPrograma = objetivos.Select(p => new
            {
                p.IdObjetivo,
                p.IdPrograma,
                p.Descricao
            }).ToList();

            var path = $"{_iwebhostenvironment.WebRootPath}\\Reports\\ObjetivosProgramaReport.rdlc";

            LocalReport localReport = new(path);

            // Adicionar o ReportDataSource ao LocalReport
            localReport.AddDataSource("DataSetObjetivosPrograma", relatorioObjetivosPrograma);

            var result = localReport.Execute(RenderType.Pdf, 1, null, "");

            return File(result.MainStream, "application/pdf", "RelatorioObjetivosPrograma.pdf");
        }
    }
}
