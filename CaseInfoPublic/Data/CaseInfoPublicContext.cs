﻿using CaseInfoPublic.Data.Mappings;
using CaseInfoPublic.Models;
using Microsoft.EntityFrameworkCore;

namespace CaseInfoPublic.Data
{
    public class CaseInfoPublicContext : DbContext
    {
        public DbSet<ProgramaModel> Programas { get; set; }
        public DbSet<IndicadorProgramaModel> IndicadoresPrograma { get; set; }
        public DbSet<ObjetivoProgramaModel> ObjetivosPrograma { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer("Data Source=.\\SQLEXPRESS;Initial Catalog=CaseInfoPublic;Integrated Security=SSPI");
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProgramaMap());
            modelBuilder.ApplyConfiguration(new IndicadorProgramaMap());
            modelBuilder.ApplyConfiguration(new ObjetivoProgramaMap());
        }
    }
}
