﻿using CaseInfoPublic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CaseInfoPublic.Data.Mappings
{
    public class ProgramaMap : IEntityTypeConfiguration<ProgramaModel>
    {
        public void Configure(EntityTypeBuilder<ProgramaModel> builder)
        {
            builder.ToTable("Programa");

            builder.HasKey(x => x.Codigo);

            builder.Property(x => x.Codigo)
                .HasMaxLength(4);

            builder.Property(x => x.Descricao)
                .IsRequired()
                .HasColumnName("Descricao")
                .HasColumnType("VARCHAR")
                .HasMaxLength(70);

            builder.Property(x => x.PublicoAlvo)
                .IsRequired()
                .HasColumnName("PublicoAlvo")
                .HasColumnType("VARCHAR")
                .HasMaxLength(70);

            builder.Property(x => x.Tipo)
                .IsRequired()
                .HasColumnName("Tipo")
                .HasColumnType("INT");

            builder.Property(x => x.ObjMilenio)
                .IsRequired()
                .HasColumnName("ObjMilenio")
                .HasColumnType("INT");

            builder.HasOne(x => x.IndicadorPrograma)
                .WithOne(x => x.Programa)
                .HasConstraintName("FK_Programa_IndicadorPrograma")
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(x => x.ObjetivosPrograma)
                .WithOne(x => x.Programa)
                .HasConstraintName("FK_Programa_ObjetivoPrograma")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
