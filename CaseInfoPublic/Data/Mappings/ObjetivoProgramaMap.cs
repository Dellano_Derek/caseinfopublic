﻿using CaseInfoPublic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CaseInfoPublic.Data.Mappings
{
    public class ObjetivoProgramaMap : IEntityTypeConfiguration<ObjetivoProgramaModel>
    {
        public void Configure(EntityTypeBuilder<ObjetivoProgramaModel> builder)
        {
            builder.ToTable("ObjetivoPrograma");

            builder.HasKey(x => x.IdObjetivo);

            builder.Property(x => x.Descricao)
                .IsRequired()
                .HasColumnName("Descricao")
                .HasColumnType("VARCHAR")
                .HasMaxLength(150);

            builder.HasOne(x => x.Programa)
                .WithMany(x => x.ObjetivosPrograma)
                .HasForeignKey(x => x.IdPrograma)
                .HasConstraintName("FK_ObjetivoPrograma_Programa")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
