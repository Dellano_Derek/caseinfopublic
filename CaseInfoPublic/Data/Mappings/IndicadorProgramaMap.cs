﻿using CaseInfoPublic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CaseInfoPublic.Data.Mappings
{
    public class IndicadorProgramaMap : IEntityTypeConfiguration<IndicadorProgramaModel>
    {
        public void Configure(EntityTypeBuilder<IndicadorProgramaModel> builder)
        {
            builder.ToTable("IndicadorPrograma");

            builder.HasKey(x => x.IdIndicador);

            builder.Property(x => x.IdIndicador)
               .ValueGeneratedOnAdd()
               .UseIdentityColumn();

            builder.Property(x => x.Descricao)
                .IsRequired()
                .HasColumnName("Descricao")
                .HasColumnType("VARCHAR")
                .HasMaxLength(150);

            builder.Property(x => x.UndMedida)
               .IsRequired()
               .HasColumnName("UndMedida")
               .HasColumnType("VARCHAR")
               .HasMaxLength(50);

            builder.Property(x => x.IndiceApuracao)
               .IsRequired()
               .HasColumnName("IndiceApuracao")
               .HasColumnType("DECIMAL")
               .HasPrecision(5, 2);

            builder.Property(x => x.IndiceDesejado)
               .IsRequired()
               .HasColumnName("IndiceDesejado")
               .HasColumnType("DECIMAL")
               .HasPrecision(5, 2);

            builder.Property(x => x.DataApuracao)
               .IsRequired()
               .HasColumnName("DataApuracao")
               .HasColumnType("SMALLDATETIME");

            builder.HasOne(x => x.Programa)
                .WithOne(x => x.IndicadorPrograma)
                .HasForeignKey<IndicadorProgramaModel>(x => x.IdPrograma)
                .HasConstraintName("FK_IndicadorPrograma_Programa")
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
