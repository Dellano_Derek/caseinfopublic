﻿namespace CaseInfoPublic.Models
{
    public class IndicadorProgramaModel
    {
        public int IdIndicador { get; set; }
        public string Descricao { get; set; }
        public string UndMedida { get; set; }
        public decimal IndiceApuracao { get; set; }
        public decimal IndiceDesejado { get; set; }
        public DateTime DataApuracao { get; set; }

        public int IdPrograma { get; set; }
        public ProgramaModel? Programa { get; set; }
    }
}
