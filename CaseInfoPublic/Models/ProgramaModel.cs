﻿using Microsoft.Extensions.Hosting;

namespace CaseInfoPublic.Models
{
    public class ProgramaModel
    {
        public int Codigo { get; set; }
        public string Descricao { get; set; }
        public string PublicoAlvo { get; set; }
        public int Tipo { get; set; }
        public int ObjMilenio { get; set; }

        public IndicadorProgramaModel? IndicadorPrograma { get; set; }
        public IList<ObjetivoProgramaModel>? ObjetivosPrograma { get; set; }

        // Dicionários para mapear valores inteiros para strings
        public readonly Dictionary<int, string> tiposNome = new() 
        {
            { 1, "Apoio" },
            { 2, "Finalístico" },
            { 3, "Especial" }
        };

        public readonly Dictionary<int, string> objetivosNome = new()
        {
            { 1, "Acabar com a Fome e a Miséria" },
            { 2, "Educação Básica de Qualidade para todos" },
            { 3, "Igualdade entre Sexos e Valorização da Mulher" },
            { 4, "Reduzir a Mortalidade Infantil" },
            { 5, "Melhorar a Saúde das Gestantes" },
            { 6, "Combater a AIDS, a Malária e outras doenças" },
            { 7, "Qualidade de Vida e Respeito ao Meio Ambiente" },
            { 8, "Todo Mundo trabalhando pelo Desenvolvimento" },
            { 9, "Outros Objetivos" }
        };

        public string? TipoNome
        {
            get
            {
                // Verifica se o tipo está mapeado no dicionário
                if (tiposNome.ContainsKey(Tipo))
                {
                    return tiposNome[Tipo];
                }
                else
                {
                    return "Tipo Desconhecido";
                }
            }
        }

        public string? ObjetivoNome
        {
            get
            {
                // Verifica se o objetivo está mapeado no dicionário
                if (objetivosNome.ContainsKey(ObjMilenio))
                {
                    return objetivosNome[ObjMilenio];
                }
                else
                {
                    return "Objetivo Desconhecido";
                }
            }
        }
    }
}
