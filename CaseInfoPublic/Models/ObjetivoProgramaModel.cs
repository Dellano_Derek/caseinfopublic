﻿namespace CaseInfoPublic.Models
{
    public class ObjetivoProgramaModel
    {
        public int IdObjetivo { get; set; }
        public string Descricao { get; set; }

        public int IdPrograma { get; set; }
        public ProgramaModel Programa { get; set; }
    }
}