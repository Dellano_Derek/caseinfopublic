﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CaseInfoPublic.Migrations
{
    public partial class InitialCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Programa",
                columns: table => new
                {
                    Codigo = table.Column<int>(type: "int", maxLength: 4, nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "VARCHAR(70)", maxLength: 70, nullable: false),
                    PublicoAlvo = table.Column<string>(type: "VARCHAR(70)", maxLength: 70, nullable: false),
                    Tipo = table.Column<int>(type: "INT", nullable: false),
                    ObjMilenio = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programa", x => x.Codigo);
                });

            migrationBuilder.CreateTable(
                name: "IndicadorPrograma",
                columns: table => new
                {
                    IdIndicador = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "VARCHAR(150)", maxLength: 150, nullable: false),
                    UndMedida = table.Column<string>(type: "VARCHAR(50)", maxLength: 50, nullable: false),
                    IndiceApuracao = table.Column<decimal>(type: "DECIMAL(5,2)", precision: 5, scale: 2, nullable: false),
                    IndiceDesejado = table.Column<decimal>(type: "DECIMAL(5,2)", precision: 5, scale: 2, nullable: false),
                    DataApuracao = table.Column<DateTime>(type: "SMALLDATETIME", nullable: false),
                    IdPrograma = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IndicadorPrograma", x => x.IdIndicador);
                    table.ForeignKey(
                        name: "FK_IndicadorPrograma_Programa",
                        column: x => x.IdPrograma,
                        principalTable: "Programa",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ObjetivoPrograma",
                columns: table => new
                {
                    IdObjetivo = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(type: "VARCHAR(150)", maxLength: 150, nullable: false),
                    IdPrograma = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjetivoPrograma", x => x.IdObjetivo);
                    table.ForeignKey(
                        name: "FK_ObjetivoPrograma_Programa",
                        column: x => x.IdPrograma,
                        principalTable: "Programa",
                        principalColumn: "Codigo",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IndicadorPrograma_IdPrograma",
                table: "IndicadorPrograma",
                column: "IdPrograma",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ObjetivoPrograma_IdPrograma",
                table: "ObjetivoPrograma",
                column: "IdPrograma");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IndicadorPrograma");

            migrationBuilder.DropTable(
                name: "ObjetivoPrograma");

            migrationBuilder.DropTable(
                name: "Programa");
        }
    }
}
