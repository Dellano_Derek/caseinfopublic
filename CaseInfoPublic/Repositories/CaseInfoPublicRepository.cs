﻿using CaseInfoPublic.Data;
using CaseInfoPublic.Models;
using Microsoft.EntityFrameworkCore;

namespace CaseInfoPublic.Repositories
{
    public class CaseInfoPublicRepository : ICaseInfoPublicRepository
    {
        private readonly CaseInfoPublicContext _context;
        public CaseInfoPublicRepository(CaseInfoPublicContext context)
        {
            _context = context; 
        }

        //Programa

        public ProgramaModel? GetPrograma(int codigo)
           => _context.Programas.AsNoTracking().FirstOrDefault(x => x.Codigo == codigo);

        public IEnumerable<ProgramaModel> GetAllProgramas()
           => _context.Programas.AsNoTracking().ToList();

        public void CreatePrograma(ProgramaModel programa)
        {
            //Não se aplica na regra de negócio
            //var programa = _context.Programas.AsNoTracking().FirstOrDefault(x => x.Codigo == codigo); 

            _context.Programas.Add(programa);
            _context.SaveChanges();
        }

        public void UpdatePrograma(int codigo, ProgramaModel programaAlt)
        {
            var programa = _context.Programas.FirstOrDefault(x => x.Codigo == codigo);
            if (programa == null)
                return;

            programa.Descricao = programaAlt.Descricao;
            programa.PublicoAlvo = programaAlt.PublicoAlvo;
            programa.Tipo = programaAlt.Tipo;
            programa.ObjMilenio = programaAlt.ObjMilenio;

            _context.Programas.Update(programa);
            _context.SaveChanges();
        }

        public void DeletePrograma(int codigo)
        {
            var programa = _context.Programas.FirstOrDefault(x => x.Codigo == codigo);
            if (programa == null)
                return;

            _context.Programas.Remove(programa);
            _context.SaveChanges();
        }

        //IndicadorPrograma

        public IndicadorProgramaModel? GetIndicadorPrograma(int idIndicador)
           => _context.IndicadoresPrograma.AsNoTracking().FirstOrDefault(x => x.IdIndicador == idIndicador);

        public IEnumerable<IndicadorProgramaModel> GetAllIndicadoresPrograma()
           => _context.IndicadoresPrograma.AsNoTracking().ToList();

        public void CreateIndicadorPrograma(IndicadorProgramaModel indicadorPrograma)
        {
                _context.IndicadoresPrograma.Add(indicadorPrograma);
                _context.SaveChanges();
        }

        public void UpdateIndicadorPrograma(int idIndicador, IndicadorProgramaModel indicadorProgramaAlt)
        {
            var indicadorPrograma = _context.IndicadoresPrograma.FirstOrDefault(x => x.IdIndicador == idIndicador);
            if (indicadorPrograma == null)
                return;

            indicadorPrograma.Descricao = indicadorProgramaAlt.Descricao;
            indicadorPrograma.UndMedida = indicadorProgramaAlt.UndMedida;
            indicadorPrograma.IndiceApuracao = indicadorProgramaAlt.IndiceApuracao;
            indicadorPrograma.IndiceDesejado = indicadorProgramaAlt.IndiceDesejado;
            indicadorPrograma.DataApuracao = indicadorProgramaAlt.DataApuracao;
            indicadorPrograma.IdPrograma = indicadorProgramaAlt.IdPrograma;

            _context.IndicadoresPrograma.Update(indicadorPrograma);
            _context.SaveChanges();
        }

        public void DeleteIndicadorPrograma(int idIndicador)
        {
            var indicadorPrograma = _context.IndicadoresPrograma.FirstOrDefault(x => x.IdIndicador == idIndicador);
            if (indicadorPrograma == null)
                return;

            _context.IndicadoresPrograma.Remove(indicadorPrograma);
            _context.SaveChanges();
        }

        //ObjetivoPrograma

        public ObjetivoProgramaModel? GetObjetivoPrograma(int idObjetivo)
          => _context.ObjetivosPrograma.AsNoTracking().FirstOrDefault(x => x.IdObjetivo == idObjetivo);

        public IEnumerable<ObjetivoProgramaModel> GetAllObjetivosPrograma()
           => _context.ObjetivosPrograma.AsNoTracking().ToList();

        public void CreateObjetivoPrograma(int idObjetivo, ObjetivoProgramaModel objetivoProgramaAlt)
        {
            var objetivoPrograma = _context.ObjetivosPrograma.AsNoTracking().FirstOrDefault(x => x.IdObjetivo == idObjetivo);

            if (objetivoPrograma == null)
            {
                objetivoPrograma = objetivoProgramaAlt;
                _context.ObjetivosPrograma.Add(objetivoPrograma);
                _context.SaveChanges();
            };
        }

        public void UpdateObjetivoPrograma(int idObjetivo, ObjetivoProgramaModel objetivoProgramaAlt)
        {
            var objetivoPrograma = _context.ObjetivosPrograma.FirstOrDefault(x => x.IdObjetivo == idObjetivo);
            if (objetivoPrograma == null)
                return;

            objetivoPrograma.Descricao = objetivoProgramaAlt.Descricao;
            objetivoPrograma.IdPrograma = objetivoProgramaAlt.IdPrograma;

            _context.ObjetivosPrograma.Update(objetivoPrograma);
            _context.SaveChanges();
        }

        public void DeleteObjetivoPrograma(int idObjetivo)
        {
            var objetivoPrograma = _context.ObjetivosPrograma.FirstOrDefault(x => x.IdObjetivo == idObjetivo);
            if (objetivoPrograma == null)
                return;

            _context.ObjetivosPrograma.Remove(objetivoPrograma);
            _context.SaveChanges();
        }
    }
}
