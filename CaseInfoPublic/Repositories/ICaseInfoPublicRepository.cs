﻿using CaseInfoPublic.Models;

namespace CaseInfoPublic.Repositories
{
    public interface ICaseInfoPublicRepository
    {
        ProgramaModel? GetPrograma(int codigo);
        IEnumerable<ProgramaModel> GetAllProgramas();
        void CreatePrograma(ProgramaModel programa);
        void UpdatePrograma(int codigo, ProgramaModel programaAlt);
        void DeletePrograma(int codigo);
        IndicadorProgramaModel? GetIndicadorPrograma(int idIndicador);
        IEnumerable<IndicadorProgramaModel> GetAllIndicadoresPrograma();
        void CreateIndicadorPrograma(IndicadorProgramaModel indicadorPrograma);
        void UpdateIndicadorPrograma(int idIndicador, IndicadorProgramaModel indicadorProgramaAlt);
        void DeleteIndicadorPrograma(int idIndicador);
        ObjetivoProgramaModel? GetObjetivoPrograma(int idObjetivo);
        IEnumerable<ObjetivoProgramaModel> GetAllObjetivosPrograma();
        void CreateObjetivoPrograma(int idObjetivo, ObjetivoProgramaModel objetivoProgramaAlt);
        void UpdateObjetivoPrograma(int idObjetivo, ObjetivoProgramaModel objetivoProgramaAlt);
        void DeleteObjetivoPrograma(int idObjetivo);
    }
}